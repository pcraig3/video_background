	<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Megan From Texas</title>
  <meta name="description" content="Obviously not a Canadian.">
  <meta name="author" content="Paul Craig">
  <!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <style>

  body 
  {
    color: white;
    font-size: 2.5em;
    font-family: tahoma, arial, sans-serif;
  }

  #video_background { 
    position: absolute; 
    bottom: 0px; 
    right: 0px; 
    min-width: 100%; 
    min-height: 100%; 
    width: auto; 
    height: auto; 
    z-index: -1000; 
    overflow: hidden; 
  }

  
  #video_pattern { 
    background-image: url(./pattern.png); 
    position: fixed; 
    opacity: 0.8; 
    left: 0px; 
    top: 0px; 
    width: 100%; 
    height: 100%; 
    z-index: 1; 
    } 

  </style>

  
</head>
<body>

  <h1>She's a Texan.</h1>
  
  <video id="video_background" preload="auto" autoplay="true" loop="loop" muted="" volume="0"> 
    <source src="v/m.mp4" type="video/mp4"> 
    <!--source src="v/m.webm" type="video/webm"--> 
    <source src="v/m.ogv" type="video/ogg ogv" ;="" codecs="theora, vorbis"> 
        Video not supported 
  </video>


</body>
</html>
